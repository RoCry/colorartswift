//
//  ColorArt.swift
//  ColorArtSwift
//
//  Created by RoCry on 8/19/16.
//  Copyright © 2016 appao. All rights reserved.
//

// heavily inspire from https://github.com/panicinc/ColorArt

import UIKit

private let kColorThresholdMinimumPercentage = 0.01
private let kScaledImageUnit: CGFloat = 30

private extension UIColor {
    func isDarkColor() -> Bool {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let lum = 0.2126 * r + 0.7152 * g + 0.0722 * b;
        
        if lum < 0.5 {
            return true
        }
        
        return false
    }
    
    func isDistinct(_ compareColor: UIColor) -> Bool {
        var r1: CGFloat = 0
        var g1: CGFloat = 0
        var b1: CGFloat = 0
        var a1: CGFloat = 0
        
        var r2: CGFloat = 0
        var g2: CGFloat = 0
        var b2: CGFloat = 0
        var a2: CGFloat = 0
        
        getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        compareColor.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        
        let threshold: CGFloat = 0.25; //.15
        
        if fabs(r1 - r2) > threshold
            || fabs(g1 - g2) > threshold
            || fabs(b1 - b2) > threshold
            || fabs(a1 - a2) > threshold {
            // check for grays, prevent multiple gray colors
            
            if fabs(r1 - g1) < 0.03 && fabs(r1 - b1) < 0.03 {
                if fabs(r2 - g2) < 0.03 && fabs(r2 - b2) < 0.03 {
                    return false
                }
            }
            
            return true
        }
        
        return false
    }
    
    func colorWithMinimumSaturation(_ minSaturation: CGFloat) -> UIColor {
        var hue: CGFloat = 0
        var saturation: CGFloat = 0
        var brightness: CGFloat = 0
        var alpha: CGFloat = 0
        
        getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        
        if saturation < minSaturation {
            return UIColor(hue: hue, saturation: minSaturation, brightness: brightness, alpha: alpha)
        }
        
        return self
    }
    
    func isBlackOrWhite() -> Bool {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        if r > 0.91 && g > 0.91 && b > 0.91 {
            return true // white
        }
        
        if r < 0.09 && g < 0.09 && b < 0.09 {
            return true // black
        }
        
        return false
    }
    
    func isContrastingColor(_ compareColor: UIColor) -> Bool {
        var r1: CGFloat = 0
        var g1: CGFloat = 0
        var b1: CGFloat = 0
        var a1: CGFloat = 0
        
        var r2: CGFloat = 0
        var g2: CGFloat = 0
        var b2: CGFloat = 0
        var a2: CGFloat = 0
        
        getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        compareColor.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        
        let lum1 = 0.2126 * r1 + 0.7152 * g1 + 0.0722 * b1
        let lum2 = 0.2126 * r2 + 0.7152 * g2 + 0.0722 * b2
        
        var contrast: CGFloat = 0
        
        if lum1 > lum2 {
            contrast = (lum1 + 0.05) / (lum2 + 0.05)
        } else {
            contrast = (lum2 + 0.05) / (lum1 + 0.05)
        }
        
        //return contrast > 3.0; //3-4.5 W3C recommends 3:1 ratio, but that filters too many colors
        return contrast > 1.6
    }
}

private struct CountedColor {
    let color: UIColor
    let count: Int
}

extension CountedColor: Comparable {
    static func <(lhs: CountedColor, rhs: CountedColor) -> Bool {
        return lhs.count > rhs.count
    }
    
    static func ==(lhs: CountedColor, rhs: CountedColor) -> Bool {
        return lhs.count == rhs.count
    }
}

private extension UIImage {
    func scaleIfNeed(_ size: CGSize) -> UIImage {
        if self.size.width < size.width && self.size.height < size.height {
            return self
        }
        
        var newSize = size
        let ratio = newSize.height/newSize.width
        
        if newSize.width > size.width {
            newSize = CGSize(width: size.width, height: size.width * ratio)
        }
        
        if newSize.height > size.height {
            newSize = CGSize(width: size.height/ratio, height: size.height)
        }
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        draw(in: CGRect(origin: .zero, size: newSize))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
}

class AOColorArt {
    private let scaledImage: UIImage
    private var backgroundColor: UIColor
    
    init(image: UIImage, defaultBackgroundColor: UIColor = .white) {
        self.scaledImage = image.scaleIfNeed(CGSize(width: kScaledImageUnit, height: kScaledImageUnit))
        self.backgroundColor = defaultBackgroundColor
    }
    
    func generate() -> (background: UIColor, primary: UIColor, secondary: UIColor, detail: UIColor) {
        let (backgroundColor, imageColors) = findEdgeColor(image: self.scaledImage)
        
        if let backgroundColor = backgroundColor {
            self.backgroundColor = backgroundColor
        }
        
        let isDarkBackground = self.backgroundColor.isDarkColor()
        let defaultColor: UIColor = isDarkBackground ? .white : .black
        
        guard let imageColorsUnwrap = imageColors else {
            return (self.backgroundColor, defaultColor, defaultColor, defaultColor)
        }
        
        let colors = findTextColor(colors: imageColorsUnwrap,
                    backgroundColor: self.backgroundColor,
                       defaultColor: defaultColor)
        
        return (self.backgroundColor, colors.0, colors.1, colors.2)
    }
}

private extension AOColorArt {
    func findEdgeColor(image: UIImage) -> (UIColor?, NSCountedSet?) {
        guard
            let pixelData = image.cgImage?.dataProvider?.data,
            let data = CFDataGetBytePtr(pixelData) else {
                return (nil, nil)
        }
        
        let pixelsWidth = Int(image.size.width * image.scale)
        let pixelsHeight = Int(image.size.height * image.scale)
        
        let imageColors = NSCountedSet(capacity: pixelsWidth * pixelsHeight)
        let leftEdgeColors = NSCountedSet(capacity: pixelsHeight)
        
        var searchColumnX = 0
        
        for x in 0..<pixelsWidth {
            for y in 0..<pixelsHeight {
                let pixelInfo = ((pixelsWidth * y) + x) * 4
                
                // is rgb right?
                let b = CGFloat(data[pixelInfo]) / CGFloat(255.0)
                let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
                let r = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
                let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)
                
                let color = UIColor(red: r, green: g, blue: b, alpha: a)
                
                if x == searchColumnX {
                    if a > 0.5 {
                        leftEdgeColors.add(color)
                    }
                }
                
                if a > 0.5 {
                    imageColors.add(color)
                }
            }
            
            if leftEdgeColors.count == 0 {
                searchColumnX += 1
            }
        }
        
        var sortedColors = [CountedColor]()
        for curColor in leftEdgeColors {
            let colorCount = leftEdgeColors.count(for: curColor)
            let randomColorsThreshold = Int(Double(pixelsHeight) * kColorThresholdMinimumPercentage)
            
            if colorCount <= randomColorsThreshold {
                continue
            }
            
            sortedColors.append(CountedColor(color: curColor as! UIColor, count: colorCount))
        }
        
        sortedColors.sort()
        
        var proposedEdgeColor: CountedColor?
        for nextProposedColor in sortedColors {
            if let curProposedEdgeColor = proposedEdgeColor {
                if Double(nextProposedColor.count) / Double(curProposedEdgeColor.count) > 0.3 {
                    if !nextProposedColor.color.isBlackOrWhite() {
                        proposedEdgeColor = nextProposedColor
                        break
                    }
                } else {
                    // reached color threshold less than 40% of the original proposed edge color so bail
                    break
                }
            } else {
                proposedEdgeColor = nextProposedColor
            }
        }
        
        return (proposedEdgeColor?.color, imageColors)
    }
    
    func findTextColor(colors: NSCountedSet, backgroundColor: UIColor, defaultColor: UIColor) -> (UIColor, UIColor, UIColor) {
        var sortedColors = [CountedColor]()
        
        let findDarkTextColor = !backgroundColor.isDarkColor()
        
        for curColor in colors {
            let curColor = (curColor as! UIColor).colorWithMinimumSaturation(0.15)
            
            if curColor.isDarkColor() == findDarkTextColor {
                let colorCount = colors.count(for: curColor)

//                 prevent using random colors, threshold should be based on input image size
//                if colorCount <= 2 {
//                    continue
//                }

                sortedColors.append(CountedColor(color: curColor, count: colorCount))
            }
        }
        
        sortedColors.sort()
        
        var primaryColor: UIColor?
        var secondaryColor: UIColor?
        var detailColor: UIColor?
        
        for countedColor in sortedColors {
            let color = countedColor.color
            
            if primaryColor == nil {
                if color.isContrastingColor(backgroundColor) {
                    primaryColor = color
                }
            } else if secondaryColor == nil {
                if !primaryColor!.isDistinct(color) || !color.isContrastingColor(backgroundColor) {
                    continue
                }
                
                secondaryColor = color
            } else if detailColor == nil {
                if !secondaryColor!.isDistinct(color)
                    || !primaryColor!.isDistinct(color)
                    || !color.isContrastingColor(backgroundColor) {
                    continue
                }
                
                detailColor = color
                
                break
            }
        }
        
        return (primaryColor ?? defaultColor, secondaryColor ?? defaultColor, detailColor ?? defaultColor)
    }
}
