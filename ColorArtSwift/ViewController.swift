//
//  ViewController.swift
//  ColorArtSwift
//
//  Created by RoCry on 8/19/16.
//  Copyright © 2016 appao. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var secondaryLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var primaryButton: UIButton!
    
    var images: [UIImage]!
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        images = (1...22)
            .map {
                "demo\($0)"
            }
            .map {
                return UIImage(named: $0)!
        }
        
        setupForImage(image: images[currentIndex])
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func changeAction(_ sender: AnyObject) {
        currentIndex = (currentIndex+1) % images.count
        setupForImage(image: images[currentIndex])
    }
    
    func setupForImage(image: UIImage) {
        DispatchQueue.global(qos: .background).async {
            let begin = Date()
            let art = AOColorArt(image: image, defaultBackgroundColor: .black)
            let colors = art.generate()
            
            DispatchQueue.main.async {
                print("time cost: \(String(format: "%.2fms", Date().timeIntervalSince(begin)*1000))")
                self.imageView.image = image
                self.primaryButton.setTitleColor(colors.primary, for: .normal)
                self.secondaryLabel.textColor = colors.secondary
                self.detailLabel.textColor = colors.detail
                self.view.backgroundColor = colors.background
            }
        }
        
    }
}

